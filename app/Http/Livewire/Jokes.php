<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Http;
use Livewire\Component;

class Jokes extends Component
{
    public string $jokeFeed = 'sv443';
    public int $jokeLimit = 1;
    protected array $jokes = [];

    protected function jokeFeed(): array
    {

        switch ($this->jokeFeed) {
            case  'icndb':
                return Http::get('http://api.icndb.com/jokes/random/'.$this->jokeLimit)->json();
            case 'sv443':
                return Http::get('https://v2.jokeapi.dev/joke/Any?type=single&amount='.$this->jokeLimit)->json();
            default:
                return [];
        }

    }

    protected function getJokes(): array
    {
        if (array_key_exists('value', $this->jokeFeed())) {
            foreach ($this->jokeFeed()['value'] as $joke) {
                array_push($this->jokes, ['joke' => $joke['joke']]);
            }
        } else {
            if (array_key_exists('jokes', $this->jokeFeed())) {
                foreach ($this->jokeFeed()['jokes'] as $joke) {
                    array_push($this->jokes, ['joke' => $joke['joke']]);
                }
            } else {
                array_push($this->jokes, ['joke' => $this->jokeFeed()['joke']]);
            }
        }

        return $this->jokes;
    }

    public function render()
    {
        return view('livewire.jokes', [
            'jokes' => $this->getJokes()
        ]);
    }
}
