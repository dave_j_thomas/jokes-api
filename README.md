## About Jokes-API

I chose to use Laravel with livewire as the php frameworks, Tailwind CSS for the style, the style is one area that I
could have spent a lot more time on, but it does the job.

other changes that could be made is to replace the logic in the getJokes function with resources.

## Learn more about Larevel

- [Laravel ](https://laravel.com/)

## Learn more about Livewire

- [Livewire](https://laravel-livewire.com/)

## Learn more about Tailwind

- [TailwindCSS](https://tailwindcss.com/)
