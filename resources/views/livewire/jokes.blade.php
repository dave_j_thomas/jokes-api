<div>

    <div class="mt-1 bg-gray-800 shadow sm:rounded-lg">
        <div class="flex items-center justify-evenly">
            <div>
                <label for="joke-feed" class="text-sm text-white">Select Joke Feed</label>
                <select name="joke-feed" class="px-6 py-2 rounded"
                        wire:model="jokeFeed">
                    <option value="icndb">icndb.com</option>
                    <option value="sv443">sv443.net</option>
                </select>
            </div>

            <div>
                <label for="joke-limit" class="text-sm text-white">How Many Jokes</label>
                <select name="joke-limit" class="px-6 py-2 rounded"
                        wire:model="jokeLimit">
                    @for($i = 1; $i<=20; $i++)
                        <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
            </div>
        </div>
    </div>

    @if(count($jokes)>0)
        @foreach($jokes as $joke)
            <div class="mt-8 bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                <div class="p-6">
                    <div class="mt-2 text-white text-sm">
                        {{ $joke['joke'] }}
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="mt-8 bg-gray-800 overflow-hidden shadow sm:rounded-lg">
            <div class="grid grid-cols-1 md:grid-cols-2">
                <div class="p-6">
                    <div class="ml-12">
                        <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                            No Jokes Returned, try again
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="text-white"
         wire:loading.delay>
        Fetching Jokes...
    </div>
</div>
